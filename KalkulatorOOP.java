import java.io.*;
import java.util.*;

class operator{
	float bilanganPertama, bilanganKedua;
	float tambah(float bilanganPertama, float bilanganKedua){
		return bilanganPertama+bilanganKedua;
	}
	float kurang(float bilanganPertama, float bilanganKedua){
		return bilanganPertama - bilanganKedua;
	}
	double kali(float bilanganPertama, float bilanganKedua){
		return bilanganPertama*bilanganKedua;
	}
	double bagi (float bilanganPertama, float bilanganKedua){
		return bilanganPertama/bilanganKedua;
	}
}

class menu{
	void menu(){
		System.out.println("1. Penjumlahan");
		System.out.println("2. Pengurangan");
		System.out.println("3. Perkalian");
		System.out.println("4. Pembagian");
		System.out.println("0. Keluar");
	}
}

public class KalkulatorOOP{
	public static void main (String [] args){
	String pilihperulangan = null;
	int perulangan = 0;
do{
		operator kalkulator = new operator();
		menu tampilkanmenu = new menu();
		BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(System.in));
		String input = null;
		
		System.out.println("\nKalkulator");
		tampilkanmenu.menu();
		System.out.print("Masukkan pilihan Anda = ");
		try{
			input = bufferedreader.readLine();
			try{
				int pilih = Integer.parseInt(input);
				if (pilih==0)
						break;
				
				System.out.print("Masukkan bilangan pertama = ");
				String inputBilanganPertama = null;
				try{
					inputBilanganPertama = bufferedreader.readLine();
				}
				catch (IOException error){
					System.out.println("Anda salah memasukkan format data");
				}
				
				System.out.print("Masukkan bilangan kedua = ");
				String inputBilanganKedua = null;
				try{
					inputBilanganKedua = bufferedreader.readLine();
				}
				catch (IOException error){
					System.out.println("Anda salah memasukkan format data");
				}
				try{
					float bilanganPertama = Float.parseFloat(inputBilanganPertama);
					float bilanganKedua = Float.parseFloat(inputBilanganKedua);
					kalkulator.bilanganPertama = bilanganPertama;
					kalkulator.bilanganKedua = bilanganKedua;
					switch(pilih){
					case 0:
						break;
					case 1:
						System.out.println("Hasil penjumlahan = " + kalkulator.bilanganPertama+" + "+ kalkulator.bilanganKedua+" = " +kalkulator.tambah(kalkulator.bilanganPertama,kalkulator.bilanganKedua));
						break;
					case 2:
						System.out.println("Hasil pengurangan = " + kalkulator.bilanganPertama+" - "+ kalkulator.bilanganKedua+" = " +kalkulator.kurang(kalkulator.bilanganPertama,kalkulator.bilanganKedua));
						break;
					case 3:
						System.out.println("Hasil perkalian = " + kalkulator.bilanganPertama+" * "+ kalkulator.bilanganKedua+" = " +kalkulator.kali(kalkulator.bilanganPertama,kalkulator.bilanganKedua));
						break;
					case 4:
						System.out.println("Hasil pembagian = " + kalkulator.bilanganPertama+" / "+ kalkulator.bilanganKedua+" = " +kalkulator.bagi(kalkulator.bilanganPertama,kalkulator.bilanganKedua));
						break;
					default:
						System.out.println("Anda salah memasukkan pilihan");
						break;
					}
				}
				catch(NumberFormatException e){
					System.out.println("Anda salah memasukkan format data");
				}
			}
			catch(NumberFormatException e){
					System.out.println("Anda salah memasukkan format data");
			}
	}
	catch (IOException error){
		System.out.println("Anda salah memasukkan format data");
	}System.out.println("Apakah Anda ingin mengulangi proses?");
	        System.out.println("1. yes 2. no");
			System.out.print("Masukkan pilihan anda = ");
	        try {
                pilihperulangan= bufferedreader.readLine();
                try  {
                    perulangan = Integer.parseInt(pilihperulangan);
                    if (perulangan ==1) {
                    	perulangan = 1;
                    }
                    else if (perulangan == 2){
                    	perulangan = -1;
                    }
                    else
                    	System.out.println("Anda salah memasukkan pilihan");
                }
                catch(NumberFormatException e){
                	System.out.println("Anda salah memasukkan format data");
                }
            }
	        catch(IOException error){
            	System.out.println("salah memasukkan pilihan"+error.getMessage());
	        } 
		} while(perulangan==1);
	}
}