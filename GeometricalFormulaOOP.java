import java.io.*;

class menu{
	static void bangunruangduadimensi(){
		System.out.println("Jenis Bangun Ruang Dua Dimensi");
		System.out.println("1. Persegi");
		System.out.println("2. Persegi Panjang");
		System.out.println("3. Segitiga");
		System.out.println("4. Lingkaran");
		System.out.println("5. Belah Ketupat");
		System.out.println("6. Jajar Genjangan");
		System.out.println("7. Trapesium");
		System.out.println("0. Keluar");
	}
	
	static void bangunruangtigadimensi(){
		System.out.println("Jenis Bangun Ruang Tiga Dimensi");
		System.out.println("1. Balok");
		System.out.println("2. Kubus");
	    System.out.println("3. Prisma Segitiga");
	    System.out.println("4. Limas Segiempat");		    
		System.out.println("5. Tabung");
	    System.out.println("6. Kerucut");
	    System.out.println("7. Bola");
		System.out.println("0. Keluar");
	}
}


class bangunRuangDuaDimensi{
	static void rumusPersegi() {
	        System.out.print("Masukan Panjang Sisi = ");
	        BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(System.in));
	        String inputside = null;
	        try {
	            inputside = bufferedreader.readLine();
	            try  {
		            float side = Float.parseFloat(inputside);
		            double large = Math.pow(side,2);
		            float roving = 4*side;
		            System.out.println("Luas Persegi = " + large);
		            System.out.println("Keliling persegi = "+roving);
		        }
		        catch(NumberFormatException e) {
		            System.out.println("Anda Salah Memasukkan Format Data");
		        }
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	    }
		
	static void rumusPersegiPanjang() {
	        System.out.print("Masukan Panjang Sisi = ");
	        BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(System.in));
	        String inputPanjang = null;
	        try {
	            inputPanjang = bufferedreader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
			System.out.print("Masukan Lebar Sisi = ");
	        String inputLebar = null;
	        try {
	            inputLebar = bufferedreader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
			try{
		        float Panjang = Float.parseFloat(inputPanjang);
				float Lebar = Float.parseFloat(inputLebar);
				double Luas = Panjang*Lebar;
				float Keliling = 2*(Panjang+Lebar);
				System.out.println("Luas Persegi = " + Luas);
				System.out.println("Keliling persegi = "+ Keliling);
		    }
		    catch(NumberFormatException e) {
				System.out.println("Anda Salah Memasukkan Format Data");
			}
	    }
	
	static void rumusSegitiga() {
	        System.out.print("Masukan Panjang Sisi pertama = ");
	        BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(System.in));
	        String inputSisi1 = null;
	        try {
	            inputSisi1 = bufferedreader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }

	        System.out.print("Masukan Panjang Sisi kedua = ");
	        String inputSisi2 = null;
	        try {
	            inputSisi2 = bufferedreader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        
	        System.out.print("Masukan Panjang Sisi ketiga = ");
	        String inputSisi3 = null;
	        try {
	            inputSisi3 = bufferedreader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }

	        try  {
	            float Sisi1 = Float.parseFloat(inputSisi1);
	            float Sisi2 = Float.parseFloat(inputSisi2);
	            float Sisi3 = Float.parseFloat(inputSisi3);
	            float keliling = Sisi1+Sisi2+Sisi3;
	            double s = 0.5*keliling;
	            double luas = Math.sqrt(s*(s-Sisi1)*(s-Sisi2)*(s-Sisi3));
	            System.out.println("keliling segitiga = " + keliling);
	            System.out.println("Luas segitiga = " + luas);
	               
	        }
	        catch(NumberFormatException e) {
	            System.out.println("Anda Salah Memasukkan Format Data");
	        }
	    }
	
	static void rumusLingkaran(){
	    	System.out.print("Masukkan Panjang Jari-jari lingkaran = ");
	    	BufferedReader bufferedreader = new BufferedReader( new InputStreamReader (System.in));
	    	String inputJarijari = null;
	    	try{
	    		inputJarijari = bufferedreader.readLine();
	    	}
	    	catch(IOException error){
	    		System.out.println("Error Input " + error.getMessage());
	    	}
	    	
	    	try{
	    		float jarijari = Float.parseFloat(inputJarijari);
	    		double keliling= 2*Math.PI*jarijari;
	    		double luas = Math.PI*Math.pow(jarijari, 2);
	    		System.out.println("Luas Lingkaran = " + luas);
	    		System.out.println("Keliling Lingkaran = "+ keliling);
	    	}
	    	catch(NumberFormatException e) {
	    		System.out.println("Anda Salah Memasukkan Format Data");
	    	}
	    }
	
	static void rumusBelahKetupat(){
		System.out.print("Masukkan Panjang Diagonal Pertama = ");
	    	BufferedReader bufferedreader = new BufferedReader( new InputStreamReader (System.in));
	    	String inputdiagonal1 = null;
	    	try{
	    		inputdiagonal1 = bufferedreader.readLine();
	    	}
	    	catch(IOException error){
	    		System.out.println("Error Input " + error.getMessage());
	    	}
	    	
	    	System.out.print("Masukkan Panjang Diagonal Kedua = ");
	    	String inputdiagonal2= null;
	    	try{
	    		inputdiagonal2 = bufferedreader.readLine();
	    	}
	    	catch(IOException error){
	    		System.out.println("Error Input " + error.getMessage());
	    	}
	    	
	    	try{
	    		float diagonal1 = Float.parseFloat(inputdiagonal1);
	    		float diagonal2 = Float.parseFloat(inputdiagonal2);
	    		double luas = diagonal1*diagonal2;
	    		double sisi = Math.sqrt(Math.pow(0.5*diagonal1,2)+Math.pow(0.5*diagonal2, 2));
	    		double keliling = 4*sisi;
	    		System.out.println("Luas Belah Ketupat = " + luas);
	    		System.out.println("Keliling Belah Ketupat = "+ keliling);
	    	}
	    	catch(NumberFormatException e) {
	    		System.out.println("Anda Salah Memasukkan Format Data");
	    	}
	}
	
	static void rumusJajargenjang(){
		System.out.print("Masukkan Panjang alas jajar genjang = ");
	    	BufferedReader bufferedreader = new BufferedReader (new InputStreamReader(System.in));
	    	String inputAlas = null;
	    	try{
	    		inputAlas = bufferedreader.readLine();
	    	}
	    	catch(IOException error){
	    		System.out.println("Input Error "+ error.getMessage());
	    	}
	    	
	    	System.out.print("Masukkan Panjang Sisi miring jajargenjang = ");
	    	String inputSisimiring = null;
	    	try{
	    		inputSisimiring = bufferedreader.readLine();
	    	}
	    	catch(IOException error){
	    		System.out.println("Input Error " + error.getMessage());
	    	}
	    	
	    	System.out.print("Masukkan Tinggi Jajar genjang = ");
	    	String inputTinggi = null;
	    	try{
	    		inputTinggi= bufferedreader.readLine();
	    	}
	    	catch(IOException error){
	    		System.out.println("Input Error " + error.getMessage());
	    	}
	    	try{
	    		float alas = Float.parseFloat(inputAlas);
	    		float sisimiring = Float.parseFloat(inputSisimiring);
	    		float tinggi= Float.parseFloat(inputTinggi);
	    		double luas = 0.5*tinggi*alas;
	    		double keliling= 2*(alas+sisimiring);
	    		System.out.println("Luas Jajajr genjang = "+ luas);
	    		System.out.println("Keliling Jajar genjang = "+ keliling);
	    	}
	    	catch(NumberFormatException e){
	    		System.out.println("Anda Salah Memasukkan Format Data");
	    	}
	}
	
	static void rumusTrapesium(){
	    	System.out.print("Masukkan Panjang sisi pertama (sisi sejajar) = alas = ");
	    	BufferedReader bufferedreader = new BufferedReader (new InputStreamReader(System.in));
	    	String inputAlas = null;
	    	try{
	    		inputAlas = bufferedreader.readLine();
	    	}
	    	catch(IOException error){
	    		System.out.println("Input Error "+ error.getMessage());
	    	}
	    	
	    	System.out.print("Masukkan Panjang sisi kedua (sisi sejajar) = ");
	    	String inputSisi2 = null;
	    	try{
	    		inputSisi2 = bufferedreader.readLine();
	    	}
	    	catch(IOException error){
	    		System.out.println("Input Error "+ error.getMessage());
	    	}
	    	
	    	System.out.print("Masukkan Panjang sisi ketiga = ");
	    	String inputSisi3 = null;
	    	try{
	    		inputSisi3 = bufferedreader.readLine();
	    	}
	    	catch(IOException error){
	    		System.out.println("Input Error "+ error.getMessage());
	    	}
	    	
	    	System.out.print("Masukkan Panjang sisi keempat = ");
	    	String inputSisi4 = null;
	    	try{
	    		inputSisi4 = bufferedreader.readLine();
	    	}
	    	catch(IOException error){
	    		System.out.println("Input Error "+ error.getMessage());
	    	}
	    	
	    	System.out.print("Masukkan Tinggi trapesium = ");
	    	String inputTinggi= null;
	    	try{
	    		inputTinggi = bufferedreader.readLine();
	    	}
	    	catch(IOException error){
	    		System.out.println("Input Error "+ error.getMessage());
	    	}
	    	
	    	try{
	    		float alas = Float.parseFloat(inputAlas);
	    		float sisi2 = Float.parseFloat(inputSisi2);
	    		float sisi3 = Float.parseFloat(inputSisi3);
	    		float sisi4 = Float.parseFloat(inputSisi4);
	    		float tinggi = Float.parseFloat(inputTinggi);
	    		double luas = 0.5*(alas+sisi2)*tinggi;
	    		double keliling = alas+sisi2+sisi3+sisi4;
	    		System.out.println("Luas Trapesium = "+ luas);
	    		System.out.println("Keliling Trapesium = "+ keliling);
	    	}
	    	catch(NumberFormatException error){
	    		System.out.println("Anda Salah Memasukkan Format Data");
	    	}	
	}
	}
	
class bangunRuangTigaDimensi{
	static void rumusBalok(){
	    System.out.print("Masukkan Panjang balok = ");
	   	BufferedReader bufferedreader = new BufferedReader (new InputStreamReader(System.in));
	   	String inputlong = null;
	   	try{
			inputlong = bufferedreader.readLine();
	    }
	   	catch(IOException error){
	   		System.out.println("Error Input "+error.getMessage());
	   	}
	    	
	   	System.out.print("Masukkan Lebar balok = ");
		String inputwide = null;
		try{
    		inputwide = bufferedreader.readLine();
    	}
    	catch(IOException error){
    		System.out.println("Error Input "+error.getMessage());
    	}
	    	
    	System.out.print("Masukkan Tinggi balok = ");
    	String inputhigh= null;
    	try{
    		inputhigh = bufferedreader.readLine();
    	}
    	catch(IOException error){
    		System.out.println("Error Input "+error.getMessage());
    	}
	    	
    	try{
    		float longbeam = Float.parseFloat(inputlong);
    		float wide = Float.parseFloat(inputwide);
    		float high = Float.parseFloat(inputhigh);
    		double large = 2*((longbeam*wide)+(longbeam*high)+(wide*high));
    		double roving = 4*(longbeam+wide+high);
    		double volume = longbeam*wide*high;
    		System.out.println("Luas Permukaan balok = "+large);
    		System.out.println("Keliling Permukaan balok = "+roving);
    		System.out.println("Volume balok = "+volume);
    	}
		catch(NumberFormatException e){
    		System.out.println("Anda Salah Memasukkan Format Data");
    	}
    }
	    
	static void rumusKubus(){
	   	System.out.print("Masukkan Panjang sisi Kubus = ");
    	BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(System.in));
    	String inputside = null;
    	try{
    		inputside = bufferedreader.readLine();
    	}
    	catch(IOException error){
    		System.out.println("Error Input "+ error.getMessage());
    	}
	    	
    	try{
    		float side = Float.parseFloat(inputside);
    		double large = 6*Math.pow(side, 2);
    		double roving = 12*side;
    		double volume = Math.pow(side,3);
    		System.out.println("Luas Permukaan Kubus = "+large);
    		System.out.println("Keliling Permukaan Kubus = "+roving);
    		System.out.println("Volume Kubus = "+volume);
    	}
    	catch(NumberFormatException e){
    		System.out.println("Anda Salah Memasukkan Format Data");
    	}	
    }
	    
	static void rumusPrismaSegitiga(){
    	System.out.print("Masukan Panjang Sisi alas pertama = ");
        BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(System.in));
        String inputside1 = null;
        try {
            inputside1 = bufferedreader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }

        System.out.print("Masukan Panjang Sisi alas kedua = ");
		bufferedreader = new BufferedReader(new InputStreamReader(System.in));
        String inputside2 = null;
        try {
            inputside2 = bufferedreader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
	        
        System.out.print("Masukan Panjang Sisi alas ketiga = ");
        bufferedreader = new BufferedReader(new InputStreamReader(System.in));
        String inputside3 = null;
        try {
            inputside3 = bufferedreader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
	        
        System.out.print("Masukan Tinggi prisma = ");
        bufferedreader = new BufferedReader(new InputStreamReader(System.in));
        String inputhigh = null;
        try {
            inputhigh = bufferedreader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }

        try  {
            float side1 = Float.parseFloat(inputside1);
            float side2 = Float.parseFloat(inputside2);
            float side3 = Float.parseFloat(inputside3);
            float high = Float.parseFloat(inputhigh);
            float roving = 2*(side1+side2+side3)+3*high;
            double s = 0.5*roving;
            double largetriangle = Math.sqrt(s*(s-side1)*(s-side2)*(s-side3));
            double large =(side1+side2+side3)*high*2*largetriangle;
            double volume = largetriangle*high;
            System.out.println("keliling Prisma Segitiga = " + roving);
            System.out.println("Luas Prisma Segitiga = " + large);
            System.out.println("Volume Prisma Segitiga = "+volume);   
        }
        catch(NumberFormatException e) {
            System.out.println("Anda Salah Memasukkan Format Data");
        }
    }
	    
	static void rumusLimasSegiempat(){
	   	System.out.print("Masukkan Panjang sisi alas = ");
    	BufferedReader bufferedreader = new BufferedReader (new InputStreamReader(System.in));
		String inputsidepedestal = null;
    	try{
    		inputsidepedestal = bufferedreader.readLine();
    	}
    	catch(IOException error){
    		System.out.print("Input Error "+error.getMessage());
    	}
    	
    	System.out.print("Masukkan Tinggi Limas = ");
    	bufferedreader = new BufferedReader (new InputStreamReader(System.in));
    	String inputhigh = null;
    	try{
    		inputhigh = bufferedreader.readLine();
    	}
		catch(IOException error){
	   		System.out.print("Input Error "+error.getMessage());
	   	}
	    	
	   	try{
	   		float sidepedestal = Float.parseFloat(inputsidepedestal);
	  		float high = Float.parseFloat(inputhigh);
	   		double largepedestal = Math.pow(sidepedestal,2);
	   		double highside = Math.sqrt(Math.pow(0.5*sidepedestal,2)*Math.pow(high,2));
	   		double largeside = 0.5*sidepedestal*highside;
	   		double large = largepedestal+(4*largeside);
	   		double sidepyramid = Math.sqrt(Math.pow(highside,2)+Math.pow(0.5*sidepedestal,2));
	   		double roving = (4*sidepedestal)+(4*sidepyramid);
	   		double volume = (1/3)*largepedestal*high;
	   		System.out.println("Luas Limas Segiempat = "+ large);
	   		System.out.println("Keliling Limas Segiempat = "+ roving);
	   		System.out.println("Volume Limas Segiempat = "+ volume);
	   	}
	   	catch(NumberFormatException e){
    		System.out.println("Anda Salah Memasukkan Format Data");
    	}
    }
	    
	static void rumusTabung(){
    	System.out.print("Masukan Jari-jari = ");
		BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(System.in));
        String inputradius = null;
        try {
            inputradius = bufferedreader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
	        
        System.out.print("Masukan Tinggi tabung = ");
        bufferedreader = new BufferedReader(new InputStreamReader(System.in));
        String inputhigh = null;
        try {
            inputhigh = bufferedreader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
	        
        try{
        	float radius = Float.parseFloat(inputradius);
        	float high = Float.parseFloat(inputhigh);
        	double large = 2*Math.PI*radius*(radius+high);
        	double volume = Math.PI*Math.pow(radius,2)*high;
        	System.out.println("Luas Tabung = "+large);
        	System.out.println("Volume Tabung = "+volume);
        }
        catch(NumberFormatException e){
    		System.out.println("Anda Salah Memasukkan Format Data");
    	}	    
	}
	 
	static void rumusKerucut(){
    	System.out.print("Masukan Jari-jari = ");
		BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(System.in));
		String inputradius = null;
	    try {
            inputradius = bufferedreader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
	        
        System.out.print("Masukan Tinggi Kerucut = ");
        bufferedreader = new BufferedReader(new InputStreamReader(System.in));
        String inputhigh = null;
        try {
            inputhigh = bufferedreader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        
		try{
        	float radius = Float.parseFloat(inputradius);
        	float high = Float.parseFloat(inputhigh);
        	double side = Math.sqrt(Math.pow(high, 2)+Math.pow(radius, 2));
        	double large = Math.PI*radius*(radius+side);
        	double volume = 1/3*Math.PI*Math.pow(radius,2)*high;
        	System.out.println("Luas Tabung = "+large);
			System.out.println("Volume Tabung = "+volume);
        }
        catch(NumberFormatException e){
    		System.out.println("Anda Salah Memasukkan Format Data");
    	}
    }
	    
	static void rumusBola(){
    	System.out.print("Masukan Jari-jari = ");
		BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(System.in));
        String inputjarijari = null;
        try {
			inputjarijari = bufferedreader.readLine();
		}
	    catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
	        
        try{
	       	float jarijari= Float.parseFloat(inputjarijari);
	       	double luas= 4*Math.PI*Math.pow(jarijari, 2);
	       	double volume = (3/4)*Math.PI*Math.pow(jarijari,3);
	       	System.out.println("Luas Bola = "+luas);
	       	System.out.println("Volume Bola = "+volume);
		}
	    catch(NumberFormatException e){
	    	System.out.println("Anda Salah Memasukkan Format Data");
	    }
	}
}

public class GeometricalFormulaOOP{
	public static void main(String [] args){
		menu tampilkanmenu = new menu();
		bangunRuangDuaDimensi DuaDimensi = new bangunRuangDuaDimensi();
		bangunRuangTigaDimensi TigaDimensi = new bangunRuangTigaDimensi();
		BufferedReader bufferedreader = new BufferedReader( new InputStreamReader(System.in));
		String input = null;
		String inputpilihan = null;
		String pilihperulangan = null;
	    int perulangan = 0;
		do{
		System.out.println("Rumus Bangun Ruang Dua Dimensi dan Tiga Dimensi");
		System.out.println("1. Bangun Ruang Dua Dimensi");
		System.out.println("2. Bangun Ruang Tiga Dimensi");
		System.out.println("0. Keluar");
		System.out.print("Masukkan pilihan Anda = ");
		try{
			input = bufferedreader.readLine();
			try{
				int pilih = Integer.parseInt(input);
				switch(pilih){
				case 0:
					break;
				case 1:
					tampilkanmenu.bangunruangduadimensi();
					try{
					System.out.print("Masukkan Pilihan Anda = ");
					inputpilihan = bufferedreader.readLine();
					try{
					int pilihan = Integer.parseInt(inputpilihan);
						switch(pilihan){
						case 0:
							break;
						case 1:
							DuaDimensi.rumusPersegi();
							break;
						case 2:
							DuaDimensi.rumusPersegiPanjang();
							break;
						case 3:
							DuaDimensi.rumusSegitiga();
							break;
						case 4:
							DuaDimensi.rumusLingkaran();
							break;
						case 5:
							DuaDimensi.rumusBelahKetupat();
							break;
						case 6:
							DuaDimensi.rumusJajargenjang();
							break;
						case 7:
							DuaDimensi.rumusTrapesium();
							break;
						default:
							System.out.println("Anda salah memasukkan pilihan");
							break;
						}
					}
					catch (NumberFormatException e){
						System.out.println("Anda salah memasukkan format data");
					}
					}
					catch(NumberFormatException e){
					System.out.println("Anda salah memasukkan format data");
					}
					break;
					
				case 2:
					tampilkanmenu.bangunruangtigadimensi();
					try{
					System.out.print("Masukkan Pilihan Anda = ");
					inputpilihan = bufferedreader.readLine();
					try{
					int pilihan = Integer.parseInt(inputpilihan);
						switch(pilihan){
						case 0:
							break;
						case 1:
							TigaDimensi.rumusBalok();
							break;
						case 2:
							TigaDimensi.rumusKubus();
							break;
						case 3:
							TigaDimensi.rumusPrismaSegitiga();
							break;
						case 4:
							TigaDimensi.rumusLimasSegiempat();
							break;
						case 5:
							TigaDimensi.rumusTabung();
							break;
						case 6:
							TigaDimensi.rumusKerucut();
							break;
						case 7:
							TigaDimensi.rumusBola();
							break;
						default:
							System.out.println("Anda salah memasukkan pilihan");
							break;
						}
					}
					catch (NumberFormatException e){
						System.out.println("Anda salah memasukkan format data");
					}
					}
					catch(NumberFormatException e){
					System.out.println("Anda salah memasukkan format data");
					}
					break;
				default:
					System.out.println("Anda salah memasukkan pilihan");
					break;
				}
			}
				catch(NumberFormatException e){
					System.out.println("Anda salah memasukkan format data");
				}
			}
			catch(IOException error){
					System.out.println("Anda salah memasukkan format data"+error.getMessage());
			}
			System.out.println("Apakah Anda ingin mengulangi proses?");
	        System.out.println("1. yes 2. no");
			System.out.print("Masukkan pilihan anda = ");
	        try {
                pilihperulangan= bufferedreader.readLine();
                try  {
                    perulangan = Integer.parseInt(pilihperulangan);
                    if (perulangan ==1) {
                    	perulangan = 1;
                    }
                    else if (perulangan == 2){
                    	perulangan = -1;
                    }
                    else
                    	System.out.println("Anda salah memasukkan pilihan");
                }
                catch(NumberFormatException e){
                	System.out.println("Anda salah memasukkan format data");
                }
            }
	        catch(IOException error){
            	System.out.println("salah memasukkan pilihan"+error.getMessage());
	        } 
		} while(perulangan==1);
	}
}